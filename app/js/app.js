angular.module(
	'App',
	[
		'App.Controllers',
		'App.Service',
		'App.Filters',
		'App.Directives'
	]
);

angular.bootstrap(document, ['App']);
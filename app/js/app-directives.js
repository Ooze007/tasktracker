angular.module(
	'App.Directives',
	[]
);

angular.module('App.Directives')
	.directive(
		'myTooltip',
		[
			function() {
				// Runs during compile
				return {
					scope: {
						'tooltip': '=myTooltip'
					},
					restrict: 'A',
					template: '<a href class="my-tooltip"><span ng-transclude></span><span class="my-tooltip-popup">{{tooltip}}</span></a>',
					replace: true,
					transclude: true,
				};
			}
		]
	);
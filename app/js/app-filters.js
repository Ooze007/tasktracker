angular.module(
	'App.Filters',
	[]
);

angular.module('App.Filters')
	.filter(
		'TaskFilter',
		[
			'TaskManager',
			function (TaskManager) {
				return function(task) {
					var result = angular.copy(task);

					result.Date = new Date(result.Date);
					TaskManager.UpdateTaskTimer(result);

					return result;
				}
			}
		]
	)
	.filter(
		'TaskListFilter',
		[
			'$filter',
			function ($filter) {
				return function(tasks) {
					var result = [];

					if (angular.isArray(tasks))
						angular.forEach(tasks, function(task) {
							result.push($filter('TaskFilter')(task));
						});

					return result;
				}
			}
		]
	)
	.filter(
		"PrettyTymerFilter",
		[
			function() {
				return function(data) {
					var seconds = (data - data % 1000)/ 1000;

					var h = (seconds - seconds % 3600)/ 3600;

					seconds -= (h * 3600);

					seconds = (seconds > 0) ? seconds : (seconds * -1);

					var m = (seconds - seconds % 60)/ 60;

					seconds -= (m * 60);

					return (data < 0 ? '-' : '') + h + ':' + (m > 9 ? m : '0' + m) + ':' + (seconds > 9 ? seconds : '0' + seconds);
				}
			}
		]
	);
angular.module(
	'App.Service',
	[]
);

angular.module('App.Service')
	.provider(
		'Storage',
		function () {
			this.$get = [
				'$window',
				function($window) {
					"use strict";
					var storage = $window['localStorage'];

					function TStorage() {}

					TStorage.prototype = {
						'Get': function(key) {
							var data = storage.getItem(key);

							if (data)
								return angular.fromJson(data);

							return null;
						},
						'Set': function(key, value) {
							return storage.setItem(key, angular.toJson(value));
						},
						'Remove': function(key) {
							return storage.removeItem(key);
						},
						'Clear': function() {
							storage.clear();
						}
					};

					return new TStorage();
				}
			];
		}
	)
	.factory(
		'TaskManager',
		[
			'Storage',
			'$filter',
			function(storage, $filter) {
				"use strict";

				function TTaskManager() {
					this.taskList = []
				}

				TTaskManager.prototype = {
					'Load': function() {
						var data = storage.Get('tasks');

						if (angular.isArray(data))
							this.taskList = $filter('TaskListFilter')(data);
					},
					'Save': function() {
						storage.Set('tasks', this.taskList)
					},
					'Add': function(task) {
						this.taskList.push(task);
						this.UpdateTaskTimer(task);
						this.Save();
					},
					'AddMinites': function(task, minutes, nosave) {
						task.Date.setMinutes(task.Date.getMinutes() + minutes);
						this.UpdateTaskTimer(task);

						if (!nosave)
							this.Save();
					},
					'UpdateTaskTimer': function(task) {
						var now = new Date();
						task.Timer = task.Date - now;

						if (task.Timer < 120000)
							this.Remove(task.Timer);
					},
					'Remove': function(task) {
						if (angular.isArray(this.taskList)) {
							var pos = this.taskList.indexOf(task);
							this.taskList.splice(pos, 1);
							this.Save();
						}
					}
				};

				return new TTaskManager();
			}
		]
	);
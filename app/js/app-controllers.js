angular.module(
	'App.Controllers',
	[]
);

angular.module('App.Controllers')
	.controller(
		'MainCtrl',
		[
			'$scope',
			'$interval',
			'TaskManager',
			function($scope, $interval, TaskManager) {
				function CreateTask(minutes) {
					var now = new Date();

					now.setMinutes(now.getMinutes() + minutes);

					return {
						'Caption': '',
						'Description': '',
						'Date': now,
						'Timer': 0
					}
				}

				TaskManager.Load();
				$scope.tasks = TaskManager.taskList;
				$scope.task = CreateTask(10);

				$scope.Add = function() {
					TaskManager.Add($scope.task);
					$scope.task = CreateTask(10);
				}

				$scope.AddMinutes = function(task, minutes) {
					TaskManager.AddMinites(task, minutes);
				}

				$scope.Remove = function(task) {
					TaskManager.Remove(task);
				}

				var cancel = $interval(function(e) {
					angular.forEach($scope.tasks, function(task) {
						TaskManager.UpdateTaskTimer(task);
					});
				}, 1000);

				$scope.$on('destroy', function() {
					$interval.cancel(cancel);
				});
			}
		]
	);
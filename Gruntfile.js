module.exports = function(grunt) {
	grunt.loadNpmTasks('grunt-shell');
	grunt.loadNpmTasks('grunt-contrib-connect');

	grunt.initConfig({
		shell: {
			options : {
				stdout: true
			},
			npm_install: {
				command: 'npm install'
			},
			bower_install: {
				command: 'bower install'
			}
		},

		connect: {
			options: {
				host: '*',
				base: 'app/'
			},
			webserver: {
				options: {
					port: 9009,
					keepalive: true
				}
			},
			devserver: {
				options: {
					port: 9006,
					keepalive: true
				}
			}
		}
	});

	//installation-related
	grunt.registerTask('install', ['shell:npm_install','shell:bower_install']);

	//defaults
	grunt.registerTask('default', ['dev']);

	//development
	grunt.registerTask('dev', ['connect:devserver']);

	//server daemon
	grunt.registerTask('server', ['connect:webserver']);
}